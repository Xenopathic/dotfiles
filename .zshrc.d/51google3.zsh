function _google3_prodaccess {
  local gcert_status=$(gcertstatus --format=varz)
  local now=$(date +%s)
  local gcert_expires=$now
  if [[ $gcert_status =~ 'LOAS2:([0-9]+)' ]]; then
    gcert_expires=${match[1]}
  fi
  if (( gcert_expires - now < (8 * 3600) )); then
    gcert
  fi
}

function panic {
  echo "Access IRC on localhost:6697" >&2
  if ! read -q "_?Connect emergency security key. Done [y/N] "; then
    return 1
  fi
  gnubbyemergency --emergency
  ssh panic
  gnubbyemergency --noemergency
}

if ! [[ -e /google ]]; then
  # Not a workstation.
  return
fi

_google3_prodaccess

if ! [[ -e /google/src/head/depot/google3 ]]; then
  # prodaccess failed.
  return
fi

path=(
  /google/src/head/depot/google3/experimental/users/rmccorkell/sh/bin
  "${path[@]}"
)

for f in /etc/bash_completion.d/{g4d,p4}; do
  if [[ -f $f ]]; then
    source $f
  fi
done

for file in /google/src/head/depot/google3/experimental/users/rmccorkell/sh/source/*.zsh; do
  source "$file"
done
zstyle ":completion:*" completer _doubleslash_complete _complete _match _approximate

export P4DIFF="git --no-pager diff --no-index --color=always"
