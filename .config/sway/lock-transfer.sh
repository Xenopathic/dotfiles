#!/bin/bash

set -euo pipefail

file=/tmp/lock.$USER.png

grim $file
convert $file \
	-filter Gaussian -define filter:sigma=3 \
	-resize 25% \
	-resize 400% \
	~/.i3/lock-icon.png -gravity center -composite -matte \
	$file

if [[ -e /dev/fd/${XSS_SLEEP_LOCK_FD:--1} ]]; then
	swaylock -u -i $file {XSS_SLEEP_LOCK_FD}<&- &
	sleep 0.5
	exec {XSS_SLEEP_LOCK_FD}<&-
else
	swaylock -u -i $file &
fi

wait
