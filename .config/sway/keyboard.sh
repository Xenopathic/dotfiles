#!/bin/bash

declare -A layouts
layouts=(
	[QWERTY]='qwerty\,\,'
	[Colemak]='colemak'
)

for layout in "${!layouts[@]}"; do
	echo "$layout"
done | wofi --show dmenu | {
	read chosen
	swaymsg input '*' xkb_variant "${layouts[$chosen]}"
}
