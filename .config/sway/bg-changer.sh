#!/bin/bash

rotate_seconds=300
img_dirs=(
	/usr/share/backgrounds
)

while true; do
	img=$(find "${img_dirs[@]}" -type f -regex '.*\.\(png\|jpg\|jpeg\)' | grep -Pv "(1136x640|1366x768|768x1024|Portrait|2048x1536)" | shuf | head -1)
	swaymsg output "*" background "$img" fill
	sleep $rotate_seconds
done
