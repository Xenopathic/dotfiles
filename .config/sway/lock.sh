#!/bin/bash

set -euo pipefail

file=/tmp/lock.$USER.png

grim $file
convert $file \
	-filter Gaussian -define filter:sigma=3 \
	-resize 25% \
	-resize 400% \
	~/.i3/lock-icon.png -gravity center -composite -matte \
	$file

exec swaylock -f -u -i $file
